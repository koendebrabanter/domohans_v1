/*
 * AppGlobals.h
 *
 * Created: 9/08/2018 11:20:23
 *  Author: koen
 */ 


#ifndef APPGLOBALS_H_
#define APPGLOBALS_H_
#include <stdint.h>

#define _NOP() __asm__ __volatile__("nop")

#define FWVER  ((uint8_t*)"F0004\r\n")		// F+ always 4 digits + CRLF



#ifndef DECLARE_GLOBALS
#define EXTERNGLOBAL extern
#else
#define EXTERNGLOBAL
#endif


#define IMAGELEN 14	//the total image buffer length
#define INPUTCOUNT 108
#define OUTPUTCOUNT 72
 

EXTERNGLOBAL volatile uint16_t TckVal;
EXTERNGLOBAL volatile uint8_t TckFlag;

EXTERNGLOBAL uint8_t InputImage[IMAGELEN];		//arrays of data from-to physical hardware
EXTERNGLOBAL uint8_t OutputImage[IMAGELEN];
EXTERNGLOBAL volatile uint8_t SpiBusy;
EXTERNGLOBAL uint8_t scratch[256];				//memory for string manipulations




#endif /* APPGLOBALS_H_ */