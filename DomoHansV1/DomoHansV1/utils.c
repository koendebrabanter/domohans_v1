/*
 * utils.c
 *
 * Created: 11/08/2018 16:05:14
 *  Author: koen
 */ 
#include "AppGlobals.h"

//local prototypes
uint8_t nibbleToHexDigit(uint8_t data);


uint8_t * byteToDecString(uint8_t data)
{
	uint8_t tempdata = data;
	
	scratch[0]= (data /100) + 0x30;
	data = data%100;
	scratch[1] = (data / 10) + 0x30;
	scratch[2] = (data % 10) + 0x30;
	scratch[3] = 0;
	
	if (tempdata<10) return &scratch[2];
	if (tempdata<100) return &scratch[1];
	return scratch;
}

uint8_t * byteToHexString(uint8_t data)
{
	uint8_t Hdata = (data)>>4;
	
	scratch[0] = nibbleToHexDigit(Hdata);
	scratch[1] = nibbleToHexDigit(data);
	scratch[2] = 0;

	return scratch;
}

uint8_t nibbleToHexDigit(uint8_t data)
{
	data &=0x0f;
	if (data <10) return (data + 0x30);
	return (data +55);
}

uint8_t HexDigitToByte(uint8_t data)
{
	if (data >= '0' && data <= '9') return (data - '0');
	if (data >= 'a' && data <= 'f') return (data - 'a' + 10);
	if (data >= 'A' && data <= 'F') return (data - 'A' + 10);
	return 0; //should never happen
}