/*
 * DomoHansV1.c
 *
 * Created: 9/08/2018 11:11:57
 * Author : koen
 */ 
#define DECLARE_GLOBALS
#include <avr/io.h>
#include <avr/interrupt.h>
#include "AppGlobals.h"
#include "hardware.h"
#include "uart.h"
#include "spi.h"
#include "app.h"
#include "dataflash.h"
#include "parser.h"

void filterButton(void);
void onButtonPress(void);
void filldemodata(void);

void filldemodata(void)
{
	OutputImage[0]=0;
	OutputImage[1]=0;
	OutputImage[2]=0;
	OutputImage[3]=0;
	OutputImage[4]=0;
	OutputImage[5]=0;
	OutputImage[6]=0;
	OutputImage[7]=0;	
}

int main(void)
{
	uint8_t volatile Cycle =0;
	uint8_t volatile Cycle100 =0;
	
	Hw_PortsInit();
	Hw_TimerInit();
	UART_SerialInit();
	Spi_init();
	DF_init();
	//DF_RESET_CLR;
	//DF_CS_SET;
	TckVal=0;
	sei();
	
	//DF_TestRoutine();

	filldemodata();	//reset all outputs or set demo data
	StartSpiCycle();//do an input-output sequence

    while (1) 
    {
		if (TckFlag) 
		{
			LED_TGL;
			//every 2 ms
			TckFlag=0;
			if (Cycle==5)
			{
				AppProcess();
			}
			
			Cycle++;
			if (Cycle>=10)
			{
				 filterButton();
				 Cycle=0;
			}
			
			Cycle100++;
			if (Cycle100==49)
			{
				UpdatePulseCounters();
			}
			
			if (Cycle100>=50)//every 100ms
			{
				Cycle100=0;
				UpdateButtonTimes();
			}
			
		}
		//non-deterministic processes
		Parse();	//parse serial communication
    }
}

ISR(TIMER2_OVF_vect)
{
	//the flag is automatically cleared when the service routine is started
	TCNT2 = 5;	//to make overflow in exact 1000�sec

	TckVal++;
	TckFlag=1;

	

}

void filterButton(void)
{
	static uint8_t PrevButton=0xff;
	static uint8_t StableCount = 0;
	uint8_t snap =  GET_BUTTON;

	if (PrevButton == snap)
	{
		//status is the same
		if ((StableCount==4) && (PrevButton==0))onButtonPress();
		if (StableCount<5) StableCount++;
	}
	else
	{
		//status changed
		StableCount = 0;
		PrevButton=snap;
	}
}

void onButtonPress(void)
{
	//UartSndString((uint8_t*)"knoppeke");
	LED_TGL;
	StartSpiCycle();
}

