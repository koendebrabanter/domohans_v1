/*
 * app.h
 *
 * Created: 11/08/2018 14:37:44
 *  Author: koen
 */ 


#ifndef APP_H_
#define APP_H_

void AppProcess(void);
void UpdateButtonTimes(void);
void SetOutput (uint8_t output);
void ClearOutput (uint8_t output);
void ToggleOutput (uint8_t output);
void PulseOutput(uint8_t output);
void OnRisingEdge(uint8_t InputNumber);
void OnFallingEdge(uint8_t InputNumber);
void UpdatePulseCounters(void);



#endif /* APP_H_ */