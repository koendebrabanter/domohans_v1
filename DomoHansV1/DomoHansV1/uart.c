/*
 * uart.c
 *
 * Created: 9/08/2018 13:41:47
 *  Author: koen
 */ 

#include <avr/interrupt.h>
#include "uart.h"
#include "AppGlobals.h"


uint8_t IncrementPointer(uint8_t pointer);

//TX ringbuffer
uint8_t TxBufferData[BUFFERSIZE];
uint8_t TxBufferIn = 0;
uint8_t TxBufferOut = 0;
//RX ringbuffer
uint8_t RxBufferData[BUFFERSIZE];
uint8_t RxBufferIn = 0;
uint8_t RxBufferOut = 0;

uint8_t TxRunning=0;
uint8_t UartErrorCounter=0;




/// This function initialises the on chip UART1 for
/// communication with COM;
/// Baudrate: COM_BAUDRATE, 1 start bit, 8 data bits,
/// no parity, 1 stop bit;
void UART_SerialInit(void)
{
	uint8_t n;
	
	// setup communication buffers
	TxBufferOut = 0;
	TxBufferIn  = 0;
	RxBufferOut = 0;
	RxBufferIn  = 0;
	for (n = 0; n < BUFFERSIZE; n++)
	{
		RxBufferData[n] = 0x00;
		TxBufferData[n] = 0x00;
	}
	DISABLE_COM_SERIAL_INT;											// Disable send & receive interrupt
	FLUSH_COM_RX;													// Clear received data and flags
	
	/*
	baud	UBBRn
	9600	103,167
	19200	51,08
	38400	25,04
    */
	UBRR1H  = 0;						// Set baud rate: High byte
	UBRR1L  = 25;						// Set baud rate: Low byte
	UCSR1B  = (1<<RXEN1)|(1<<TXEN1);								// Enable receiver and transmitter
	UCSR1C  = (3<<UCSZ10);											// Set frame format: 8data, 1stop bit
	ENABLE_COM_SERIAL_INT;
}


/// Interrupt service routine for UART
/// Called when a character has been sent to
/// COM-Uart (TXCn: UART Transmit Complete)
ISR(USART1_TX_vect)
{	
	if (TxBufferOut != TxBufferIn)
	{
		UDR1  = TxBufferData[TxBufferOut];
		TxBufferData[TxBufferOut] = 0x00;
		TxBufferOut = IncrementPointer(TxBufferOut);
		ENABLE_COM_TX_SERIAL_INT;
	}
	else
	{
		DISABLE_COM_TX_SERIAL_INT;
		TxRunning=0;
	}	
}


/// Interrupt service routine for UART
/// Called when a character has been received from
/// COM-Uart (RXCn: UART Receive Complete)
ISR(USART1_RX_vect)
{
	uint8_t nError;												// The error byte
	uint8_t nChar;												// The received character
	
	nError = UCSR1A;											// Get the UART status
	nChar  = UDR1;												// Read received character
	
	//check if buffer is full (that is when RxBufferIn after increment is equal to rxBufferOut
	if (RxBufferOut != IncrementPointer(RxBufferIn))
	{
		//put byte in ringbuffer
		RxBufferData[RxBufferIn] = nChar;
		RxBufferIn=IncrementPointer(RxBufferIn);
	}	
	
	//error control
	if(nError & (1<<FE1) && (nChar != 0x00))					// If a frame error was detected
	{
		UartErrorCounter++;
	}
	if(nError & (1<<DOR1))										// If a data overruan was detected
	{
		UartErrorCounter++;
	}
}

uint8_t IncrementPointer(uint8_t pointer)
{
	pointer++;
	if (pointer >= BUFFERSIZE)
	{
		pointer  = 0;
	}
	return pointer;
}

void	StartUartTx(void)
{
	if (TxRunning) return;

	if (TxBufferOut != TxBufferIn)
	{
		//get byte from ringbuffer
		UDR1  = TxBufferData[TxBufferOut];
		TxBufferData[TxBufferOut] = 0x00;
		TxBufferOut = IncrementPointer(TxBufferOut);
		ENABLE_COM_TX_SERIAL_INT;
		TxRunning=1;
	}
	else
	{
		DISABLE_COM_TX_SERIAL_INT;
	}
	
}

uint8_t ComGetByte(void)
{
	uint8_t returnval =0;
	
	returnval = RxBufferData[RxBufferOut];
	RxBufferData[RxBufferOut] = 0x00;
	RxBufferOut = IncrementPointer(RxBufferOut);
	return returnval;
}

uint8_t ComByteAvailable(void)
{
	if (RxBufferOut != RxBufferIn)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void UartSndByte(uint8_t bChar)
{
	//check if not full
	
	//write to buffer
	TxBufferData[TxBufferIn] = bChar;
	// update pointer
	TxBufferIn = IncrementPointer(TxBufferIn);
}

void UartSndString(uint8_t * bChar)
{
	//check if not full
	
	//write to buffer
	while (*bChar)
	{
		UartSndByte(*bChar);
		bChar++;
	}

	StartUartTx();
}



