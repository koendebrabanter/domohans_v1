/*
 * spi.c
 *
 * Created: 9/08/2018 20:32:54
 *  Author: koen
 */ 

#include "AppGlobals.h"
#include <avr/interrupt.h>
#include "spi.h"
#include "hardware.h"


uint8_t ImagePointer=0;
uint8_t volatile SpiStatus;


#define ENABLE_SPI_INT  		UCSR1B |=   (1<<RXCIE1)|(1<<TXCIE1)			// Enable TX & RX serial interrupt
#define DISABLE_SPI_INT		UCSR1B &= ~((1<<RXCIE1)|(1<<TXCIE1))		// Disable TX & RX serial interrupt
/************************************************************************/
/* inputs: 74HC589
13 serial in/parallel load	IN_LOAD
12 latch clock				IN_LATCH
11 shift clock				SCK

0 IN_LOAD L to load parallel data
1 rising edge on IN_LATCH (RCK) to read inputs into latch
2 IN_LOAD h to move data into shift register. Data h is allready on output
3 start SPI cycle. each rising erdge outputs next bit



output: 74HC595
13 nOE		OUT_EN
12 RCLK		OUT_LATCH
10 nSRCLK	VCC
11 SRCLK	SCK
14 SER		MOSI

0  start SPI cycle. each rising erge outputs next bit

                                                                     */
/************************************************************************/


void Spi_init(void)
{
	SPCR =  0xD0;		//Fosc / 4
	//SPCR =  0xD1;		//Fosc / 16
	//SPCR =  0xD2;		//Fosc / 64
	//SPCR =  0xD3;		//Fosc / 128
	//demodata
	OutputImage[0]=0xa3;
	OUT_EN_CLR;	//outputs a	lways on
}

ISR(SPI_STC_vect)
{
	//SPI byte received
	SpiStatus = SPSR;
	InputImage[ImagePointer++]=SPDR;
	
	if (ImagePointer==IMAGELEN)
	{
		//complete image sent
		//put rising edge on RCLK (OUT_LATCH) to latch output data
		OUT_LATCH_SET;
		OUT_LATCH_CLR;
		//no next byte te send
		SpiBusy=0;
	}
	else
	{
		SPDR = OutputImage[ImagePointer];	//send next byte
	}
	
	
}

void StartSpiCycle(void)
{
	//starts a sequence that reads all input into "InputImage"
	//and writes the sates from "OutputImage" to outputs.
	//This sequence is interrupt controlled and takes less than 100�S to comlete
	SpiBusy=1;
	ImagePointer=0;
	
	IN_LOAD_CLR;
	IN_LATCH_SET;	//rising edge on latch wit load LOW read inputs
	IN_LOAD_SET;	//high again when we start shifting
	IN_LATCH_CLR;
	
	SPDR = OutputImage[ImagePointer];
}

void WriteOutputByte(uint8_t id,uint8_t data)
{
	OutputImage[id]=data;
}

