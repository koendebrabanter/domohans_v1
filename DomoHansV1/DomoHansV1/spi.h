/*
 * spi.h
 *
 * Created: 9/08/2018 20:33:10
 *  Author: koen
 */ 


#ifndef SPI_H_
#define SPI_H_


void Spi_init(void);
void StartSpiCycle(void);

void WriteOutputByte(uint8_t id,uint8_t data);


#endif /* SPI_H_ */