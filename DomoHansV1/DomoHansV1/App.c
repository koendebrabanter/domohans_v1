/*
 * App.c
 *
 * Created: 11/08/2018 14:37:22
 *  Author: koen
 */ 

#include "AppGlobals.h"
#include "app.h"
#include "uart.h"
#include "spi.h"
#include "utils.h"
#include "sequencer.h"

#define MAXDEBOUNCE	3

#define LONGPRESSTIME 40
#define PULSETIME 20

uint8_t lastInputImage[IMAGELEN] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};	//
uint8_t DebounceCounter[IMAGELEN][8];

uint8_t ButtonTimes[IMAGELEN][8];
uint8_t PulseCounter[OUTPUTCOUNT];

uint8_t LUT_inputTable[] =
{	//relation between location of input in inputimage (index), and virtual input number(value)
	1,  2,  3,  4,  5,  6,  7,  8, 11, 12, 13, 14, 15, 16, 17,  0,
	21, 22, 23, 24, 25, 26,  9, 10, 31, 32, 33, 34, 35, 18, 19, 20,
	41, 42, 43, 44, 27, 28, 29, 30, 51, 52, 53, 36, 37, 38, 39, 40,
	61, 62, 45, 46, 47, 48, 49, 50, 71, 54, 55, 56, 57, 58, 59, 60,
	63, 64, 65, 66, 67, 68, 69, 70, 73, 74, 75, 76, 77, 78, 79, 80,
	83, 84, 85, 86, 87, 88, 89, 72, 93, 94, 95, 96, 97, 98, 81, 82,
	103,104,105,106,107, 90, 91, 92,255,255,255,255, 99,100,101,102
};


/*
This function synchronises the input and output image in memory with the real hardware.
It debounces inputs and detects rising and falling edges.
It calls the appropriate event handers when an edge is detected
*/
void AppProcess(void)
{
	uint8_t byteId,BitId, InputNumber;
	uint8_t bitmask;
	
	/*
	note on debouncecounter
	-----------------------
	if bits are equal, nothing happens.
	if bits are not equal, debounce counter is incremented
	if debounce counter reaches top value, output takes over state of input and counter is reset.
	*/
	
	StartSpiCycle();
	while (SpiBusy);	//wait until data is read/written
	
	//iterate trough all bits, detect changes and update debouncecounters
	//call function on each rising and falling edge
	
	for (byteId=0;byteId<IMAGELEN;byteId++)
	{
		//if all 8 bits of a byte are equal, we can skip it
		if (InputImage[byteId] == lastInputImage[byteId]) continue;
		
		//at least 1 bit of the byte is different
		//we go trough all bits	
		bitmask = 0x01;
		for (BitId=0;BitId<8;BitId++)
		{
			if ((InputImage[byteId]& bitmask) != (lastInputImage[byteId]& bitmask))
			{
				//different bit found
				DebounceCounter[byteId][BitId]++;//update debounce counter
				if (DebounceCounter[byteId][BitId]>=MAXDEBOUNCE)
				{
					//reset counter
					DebounceCounter[byteId][BitId]=0;
					
					//calculate virtual input number, based on bitnumber and bytenumber
					InputNumber = (byteId <<3)+BitId;				//get location in input image
					InputNumber = LUT_inputTable[InputNumber];		//convert to virtual input number, using lookup table
					
					//call routine for rising or falling edge
					if (InputImage[byteId]& bitmask)
					{
						//call falling edge
						ButtonTimes[byteId][BitId]=0;
						seq_OnEvent(InputNumber, SEQ_EVENT_ONRELEASE);
						OnFallingEdge(InputNumber);
					}
					else
					{
						//call rising edge 						
						ButtonTimes[byteId][BitId]=1;
						seq_OnEvent(InputNumber, SEQ_EVENT_ONPRESS);
						OnRisingEdge(InputNumber);
					}
					//flip bit
					lastInputImage[byteId]^= bitmask;	
				}
			}			
			//next bitmask
			bitmask<<=1;
		}
	}
}

void OnRisingEdge(uint8_t InputNumber)
{	
	UartSndString((uint8_t*)"NR");
	UartSndString(byteToHexString(InputNumber));
	UartSndString((uint8_t*)"\r\n");	
}

void OnFallingEdge(uint8_t InputNumber)
{	
	UartSndString((uint8_t*)"NF");
	UartSndString(byteToHexString(InputNumber));
	UartSndString((uint8_t*)"\r\n");
}

void OnLongPress(uint8_t InputNumber)
{
	UartSndString((uint8_t*)"NL");
	UartSndString(byteToHexString(InputNumber));
	UartSndString((uint8_t*)"\r\n");
}

void UpdateButtonTimes(void)
{
	uint8_t byteId,BitId,InputNumber;
	
	//update counter for all input bytes
	for (byteId=0;byteId<IMAGELEN;byteId++)
	{
		//and all bits in these bytes....
		for (BitId=0;BitId<8;BitId++)
		{
			//if not zero...
			if (ButtonTimes[byteId][BitId])
			{
				//or 255.
				if (ButtonTimes[byteId][BitId]<255)
				{
					ButtonTimes[byteId][BitId]++;	//increment timer
					if (ButtonTimes[byteId][BitId]==LONGPRESSTIME)
					{
						//calculate virtual input number, based on bitnumber and bytenumber
						InputNumber = (byteId <<3)+BitId;				//get location in input image
						InputNumber = LUT_inputTable[InputNumber];		//convert to virtual input number, using lookup table
						
						seq_OnEvent(InputNumber, SEQ_EVENT_ONLONGPRESS);
						OnLongPress(InputNumber);
					}			
				}
			}
		}
	}
}

void UpdatePulseCounters(void)
{
	uint8_t outputId;
	
	//update counter for all outputs
	for (outputId=0;outputId<OUTPUTCOUNT;outputId++)
	{
		//if zero, skip
		if (PulseCounter[outputId]==0) continue;
		
		//decrement counter and take anction when 0 is reached
		PulseCounter[outputId]--;
		if (PulseCounter[outputId]==0)
		{
			//output low
			ClearOutput(outputId);
		}
		
		
						
	}
}

void SetOutput (uint8_t output)	//linear outputnumber (0x00-0x48)
{
	uint8_t ImageIndex;
	uint8_t bitmask = 0x01;
	
	ImageIndex = 13 - (output>>3);		//isolate bytenumber and transpose to index in outputimage
	bitmask <<= (output & 0x07);//create bitmast from bitnumber
	
	OutputImage[ImageIndex] |= bitmask;
}

void ClearOutput (uint8_t output)	//linear outputnumber (0x00-0x48)
{
	uint8_t ImageIndex;
	uint8_t bitmask = 0x01;
	
	ImageIndex = 13 - (output>>3);		//isolate bytenumber and transpose to index in outputimage
	bitmask <<= (output & 0x07);//create bitmast from bitnumber
	
	OutputImage[ImageIndex] &= ~bitmask;
	PulseCounter[output]=0;		//make sure a pulse is abaorted when busy
}
void ToggleOutput (uint8_t output)	//linear outputnumber (0x00-0x48)
{
	uint8_t ImageIndex;
	uint8_t bitmask = 0x01;
	
	ImageIndex = 13 - (output>>3);		//isolate bytenumber and transpose to index in outputimage
	bitmask <<= (output & 0x07);//create bitmast from bitnumber
	
	OutputImage[ImageIndex] ^= bitmask;
	PulseCounter[output]=0;		//make sure a pulse is abaorted when busy
}

void PulseOutput(uint8_t output)
{
	uint8_t ImageIndex;
	uint8_t bitmask = 0x01;
		
	ImageIndex = 13 - (output>>3);		//isolate bytenumber and transpose to index in outputimage
	bitmask <<= (output & 0x07);//create bitmast from bitnumber
		
	OutputImage[ImageIndex] |= bitmask;
	
	PulseCounter[output]=PULSETIME;
}
