/*
 * hardware.h
 *
 * Created: 9/08/2018 11:22:39
 *  Author: koen
 */ 


#ifndef HARDWARE_H_
#define HARDWARE_H_

#include <avr/io.h>

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80

//DDRx 1 = output
//DDRx 0 = input


////////////////////////////////////////////////////////////////////////////////////
// Port A

// Usage Port A.0: Unused but on RFU connector                 (OUT)
// Usage Port A.1: Unused but on RFU connector                 (OUT)
// Usage Port A.2: Unused but on RFU connector                 (OUT)
// Usage Port A.3: Unused but on RFU connector                 (OUT)
// Usage Port A.4: Unused but on RFU connector                 (OUT)
// Usage Port A.5: Unused but on RFU connector                 (OUT)
// Usage Port A.6: Unused but on RFU connector                 (OUT)
// Usage Port A.7: Unused but on RFU connector                 (OUT)

#define DDRA_SETTING			0xff
#define PORTA_SETTING			0x00

////////////////////////////////////////////////////////////////////////////////////
// Port B

// Usage Port B.0: Unused										(OUT)
// Usage Port B.1: Unused										(OUT)
// Usage Port B.2: Unused										(OUT)
// Usage Port B.3: IN_LOAD										(OUT)
// Usage Port B.4: IN_LATCH										(OUT)
// Usage Port B.5: MOSI											(OUT)
// Usage Port B.6: MISO											(IN)
// Usage Port B.7: SCK											(OUT)

#define DDRB_SETTING			0xBF
#define PORTB_SETTING			0x00

#define IN_LOAD_SET			PORTB  |=  BIT3
#define IN_LOAD_CLR			PORTB  &=  ~BIT3
#define IN_LATCH_SET		PORTB  |=  BIT4
#define IN_LATCH_CLR		PORTB  &=  ~BIT4



////////////////////////////////////////////////////////////////////////////////////
// Port C

// Usage Port C.0: OUT_EN										(OUT)
// Usage Port C.1: OUT_LATCH									(OUT)
// Usage Port C.2: Unused (JTAG)								(OUT)
// Usage Port C.3: Unused (JTAG)								(OUT)
// Usage Port C.4: Unused (JTAG)								(OUT)
// Usage Port C.5: Unused (JTAG)								(OUT)
// Usage Port C.6: switch										(IN)
// Usage Port C.7: led											(OUT)

#define DDRC_SETTING			0xbf
#define PORTC_SETTING			0x00


#define OUT_EN_SET			PORTC  |=  BIT0
#define OUT_EN_CLR			PORTC  &=  ~BIT0
#define OUT_LATCH_SET		PORTC  |=  BIT1
#define OUT_LATCH_CLR		PORTC  &=  ~BIT1
#define LED_SET				PORTC  |=  BIT7
#define LED_CLR				PORTC  &=  ~BIT7
#define LED_TGL				PINC  =  BIT7
#define GET_BUTTON			(PINC & BIT6)

////////////////////////////////////////////////////////////////////////////////////
// Port D

// Usage Port D.0: DF_SI										(IN)
// Usage Port D.1: DF_SO                                        (OUT)
// Usage Port D.2: RXD											(IN)
// Usage Port D.3: TXD											(OUT)
// Usage Port D.4: FAULT_DF_SCK									(INT)	this is the SCK from the other serial port. we put this one as input and connect a wire from te correct one to this one. The correct one is output
// Usage Port D.5: DF_RESET                                     (OUT)
// Usage Port D.6: DF_CS										(OUT)
// Usage Port D.7: unused										(OUT)

//errata
//PD4 is routed as SCK from UART0, but it is in fact the SCK from UART1, which is our serial port for external communication.
//PD4 will be set as input and we connect a wire from te correct one (PA0, pin 40) to this one. The correct one is output.

#define DF_RESET_SET		PORTD  |=  BIT5
#define DF_RESET_CLR		PORTD  &=  ~BIT5
#define DF_CS_SET			PORTD  |=  BIT6
#define DF_CS_CLR			PORTD  &=  ~BIT6


#define DDRD_SETTING			0xEA
#define PORTD_SETTING			0x01



#define ASM_NOP asm volatile ("nop" :: )


void Hw_DelayMs(uint16_t delay);
void Hw_PortsInit(void);
void Hw_TimerInit(void);




#endif /* HARDWARE_H_ */