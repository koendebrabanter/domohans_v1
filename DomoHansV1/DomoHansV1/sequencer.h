/*
 * sequencer.h
 *
 * Created: 18/11/2018 10:47:02
 *  Author: koen
 */ 


#ifndef SEQUENCER_H_
#define SEQUENCER_H_

#define SEQ_EVENT_ONPRESS		0x00
#define SEQ_EVENT_ONRELEASE		0x01
#define SEQ_EVENT_ONLONGPRESS	0x02
#define SEQ_EVENT_INVALID		0x03

void seq_OnEvent(uint8_t input, uint8_t event);


#endif /* SEQUENCER_H_ */