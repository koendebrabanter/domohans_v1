/*
 * dataflash.h
 *
 * Created: 7/10/2018 20:54:04
 *  Author: koen
 */ 


#ifndef DATAFLASH_H_
#define DATAFLASH_H_

//Dataflash opcodes

#define FlashToBuf1Transfer 		0x53	// Main memory page to buffer 1 transfer
#define Buffer1Read					0xD4
#define Buf1Write					0x84	// Buffer 1 write
#define Buf1ToFlashWE   			0x83	// Buffer 1 to main memory page program with built-in erase
#define PageRead					0xD2	// read data directly from one of the pages in main memory. 
/*
#define FlashPageRead				0x52	// Main memory page read

#define Buf1Read					0x54	// Buffer 1 read
#define FlashToBuf2Transfer 		0x55	// Main memory page to buffer 2 transfer
#define Buf2Read					0x56	// Buffer 2 read
#define StatusReg					0x57	// Status register
#define AutoPageReWrBuf1			0x58	// Auto page rewrite through buffer 1
#define AutoPageReWrBuf2			0x59	// Auto page rewrite through buffer 2
#define FlashToBuf1Compare    		0x60	// Main memory page to buffer 1 compare
#define FlashToBuf2Compare	    	0x61	// Main memory page to buffer 2 compare
#define ContArrayRead				0x68	// Continuous Array Read (Note : Only A/B-parts supported)
#define FlashProgBuf1				0x82	// Main memory page program through buffer 1


#define FlashProgBuf2				0x85	// Main memory page program through buffer 2
#define Buf2ToFlashWE   			0x86	// Buffer 2 to main memory page program with built-in erase
#define Buf2Write					0x87	// Buffer 2 write
#define Buf1ToFlash     			0x88	// Buffer 1 to main memory page program without built-in erase
#define Buf2ToFlash		         	0x89	// Buffer 2 to main memory page program without built-in erase
*/




//types of actions
enum ActionType
{
	ActionType_NoAction = 0x00,
	ActionType_Set = 0x01,
	ActionType_Clear = 0x02,
	ActionType_Toggle = 0x03,
	ActionType_SetAll = 0x04,
	ActionType_ClearAll = 0x05,
	ActionType_Pulse = 0x06,
	ActionType_Invalid = 0xff
};

//misc. functions
void DF_TestRoutine(void);
void DF_init(void);
uint16_t DF_getActivePage(void);
uint8_t* DF_getBufferPointer(void);
void DF_DeleteAll(void);
uint8_t DF_getStatus(void);

//reading and writing from/to flash and internal buffer

void DF_Data_To_Buffer(uint8_t * data, uint8_t datalen);
void DF_Buffer_To_FlashPage(uint16_t PageNumber);
void DF_ReadPage(uint16_t pagenumber);


//reading and writing from/to temp buffer
void DF_OpenEmptyBuffer(uint16_t pagenumber);
void DF_AddAction(uint8_t output, uint8_t action);
void DF_SavePage(void);




#endif /* DATAFLASH_H_ */