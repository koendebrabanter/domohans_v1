/*
 * utils.h
 *
 * Created: 11/08/2018 16:04:54
 *  Author: koen
 */ 


#ifndef UTILS_H_
#define UTILS_H_


uint8_t * byteToDecString(uint8_t data);
uint8_t * byteToHexString(uint8_t data);
uint8_t HexDigitToByte(uint8_t data);


#endif /* UTILS_H_ */