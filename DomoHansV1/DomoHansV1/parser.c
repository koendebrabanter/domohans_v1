/*
 * parser.c
 *
 * Created: 14/10/2018 13:28:02
 *  Author: koen
 */ 
#include <avr/io.h>
#include "AppGlobals.h"
#include "parser.h"
#include "uart.h"
#include "app.h"
#include "utils.h"
#include "dataflash.h"

#define PARSEBUFFERLEN	64
#define MAXCHUNK		16

void Parse_DFhandler(uint8_t* data,uint8_t datalen);
void Parse_SRThandler(uint8_t* data,uint8_t datalen);
uint8_t Parse_isHex(uint8_t* data,uint8_t datalen);
uint8_t Parse_isDec(uint8_t* data,uint8_t datalen);
uint8_t Parse_1digitHexToByte(uint8_t* data);
uint8_t Parse_2digitHexToByte(uint8_t* data);
uint16_t Parse_getPagenumber(uint8_t* data);
uint8_t Parse_getOutputnumber(uint8_t* data);
void Parse_FwVerRequest(void);
void Parse_Ping();
void Parse_reportBusy();

void ReportStatus(uint8_t outputnumber);

uint8_t parseBuffer[PARSEBUFFERLEN];
uint8_t ParsePointer=0;
uint8_t ParseCount;	//count successive bytes parsed

/*this is the main function which has to be called cyclically.
It will check wheather new data received by the serial port.
New data is appended to a linear buffer.
Parsing continues until:
-16 bytes received
-\r received
-no more data left

/n (0x0a) is ignorede, and /r(0x0d) is our EOL character

max 16 bytes are appended each time, or 
*/

void Parse(void)
{
	uint8_t databyte;
	
	ParseCount=0;	//reset counter that counts how many bytes we have parsed this time.
	while (1)
	{
		//return if maximum amount of bytes already parsed.
		if (ParseCount>=MAXCHUNK) return;
		//return if no data left
		if( ComByteAvailable()==0) return;
		
		
		//get a fresh byte
		databyte = ComGetByte();
		ParseCount++;
		
		//ignore \n
		if (databyte==0x0a) continue;	//proceed to next byte
		
		if (databyte==0x0d)
		{
			//send linear buffer to according handler
			switch (parseBuffer[0])
			{
				case '+'://this is a dataflash command
				Parse_DFhandler(parseBuffer,ParsePointer);
				break;
				
				case 'S': //this is a set/Reset/Toggle/pulse command
				case 'R':
				case 'T':
				case 'P':
				case 'G':	//get status command has same structure and is handeled here too
				Parse_SRThandler(parseBuffer,ParsePointer);
				break; 
				case 'C': //this command is a ping. Just answer with 'c'
				Parse_Ping();
				break;
				case 'F':	//firmware version request
				Parse_FwVerRequest();
				break;
			}
			
			ParsePointer=0;	//preapare for next frame
			return;
		}
		
		if (ParsePointer<PARSEBUFFERLEN)	//only if buffer not full yet
		{
			parseBuffer[ParsePointer]=databyte;
			ParsePointer++;
		}		
	}
}

void Parse_DFhandler(uint8_t* data,uint8_t datalen)
{		
	uint8_t databyte= data[1];
	uint16_t pagenumber;
	uint8_t outputnumber;
	
	switch(databyte)
	{
		case 'L': //load page "+Lxxx"
			//expecting a 3 digit hex now
			if (Parse_isHex(&data[2],3)==0) return;
			//expecting packet of 5 bytes
			if (datalen!=5) return;
			//get pagenumber
			pagenumber = Parse_getPagenumber(&data[2]);
			DF_OpenEmptyBuffer(pagenumber);
			//load page
			break;
		case 'S': //add set command to active page
			//expecting a 2 digit hex now
			if (Parse_isHex(&data[2],2)==0) return;
			//expecting packet of 4 bytes
			if (datalen!=4) return;
			//get pagenumber
			outputnumber = Parse_getOutputnumber(&data[2]);
			DF_AddAction(outputnumber,(uint8_t) ActionType_Set);
			break;
		case 'R': //add reset command to active page
			//expecting a 2 digit hex now
			if (Parse_isHex(&data[2],2)==0) return;
			//expecting packet of 4 bytes
			if (datalen!=4) return;
			//get pagenumber
			outputnumber = Parse_getOutputnumber(&data[2]);
			DF_AddAction(outputnumber, (uint8_t) ActionType_Clear);
			break;
		case 'T': //add toggle command to active page
			//expecting a 2 digit hex now
			if (Parse_isHex(&data[2],2)==0) return;
			//expecting packet of 4 bytes
			if (datalen!=4) return;
			//get pagenumber
			outputnumber = Parse_getOutputnumber(&data[2]);
			DF_AddAction(outputnumber, (uint8_t) ActionType_Toggle);
			break;
		case 'P': //add toggle command to active page
			//expecting a 2 digit hex now
			if (Parse_isHex(&data[2],2)==0) return;
			//expecting packet of 4 bytes
			if (datalen!=4) return;
			//get pagenumber
			outputnumber = Parse_getOutputnumber(&data[2]);
			DF_AddAction(outputnumber, (uint8_t) ActionType_Pulse);
			break;
		case 'X': //save current page
			if (datalen!=2) return;
			DF_SavePage();
			break;
		case 'C':	//chip erase
			if (datalen!=2) return;
			DF_DeleteAll();
			break;
		case 'B':	//read busy status
			if (datalen!=2) return;
			Parse_reportBusy();
			break;
			
		
	}
}

//converts a 3 digit hexadecimal string in a uint16
uint16_t Parse_getPagenumber(uint8_t* data)
{
	uint16_t pagenumber=0;
	uint8_t n;
	
	//we expect 3 hexadecimal digits (already checked)
	for (n=0;n<3;n++)
	{
		pagenumber<<=4;
		pagenumber+=  HexDigitToByte(*data);
		data++;
	}
	return pagenumber;
}

//converts a 2 digit hexadecimal string in a uint8
uint8_t Parse_getOutputnumber(uint8_t* data)
{
	uint8_t outputnumber=0;
	uint8_t n;
	
	//we expect 2 hexadecimal digits (already checked)
	for (n=0;n<2;n++)
	{
		outputnumber<<=4;
		outputnumber+=  HexDigitToByte(*data);
		data++;
	}
	return outputnumber;
}

//on request, just send a ping response (='c')
void Parse_Ping()
{
	UartSndString((uint8_t*)"c\r\n");
}

void Parse_reportBusy()
{
	uint8_t status;
	status = DF_getStatus();
	
	UartSndString((uint8_t*)"b");

	UartSndString(byteToHexString(status));
	UartSndString((uint8_t*)"\r\n");
	
}

void Parse_FwVerRequest()
{
	UartSndString(FWVER);
}

void Parse_SRThandler(uint8_t* data,uint8_t datalen)
{
	/*SRT packet structure:
	Set output 
	"Soo"
	Save Page 
	"Roo"
	Clear page 
	"Too"
	oo	= outputnumber( 0x00-0x48)
	 */
	uint8_t outputNumber;
	
	//sanity check
	//must be 3 chars long
	if (datalen!=3) return;
	
	//must be hexadecimal
	if (Parse_isHex(&data[1],2)==0) return;	//error: no hexadecimal output number provided
	outputNumber = Parse_2digitHexToByte(&data[1]);
	
	switch(data[0])
	{
		case 'S': SetOutput(outputNumber);
		break;
		case 'R': ClearOutput(outputNumber);
		break;
		case 'T': ToggleOutput(outputNumber);
		break;
		case 'G': //return state of this output
					ReportStatus(outputNumber);
		break;
		case 'P': //pulse output
					PulseOutput(outputNumber);
		break;
		 

	}
	
	
}

void ReportStatus(uint8_t outputnumber)
{
	uint8_t ImageIndex, state;
	uint8_t bitmask = 0x01;
	
	ImageIndex = 13 - (outputnumber>>3);		//isolate bytenumber and transpose to index in outputimage
	bitmask <<= (outputnumber & 0x07);			//create bitmask from bitnumber
	
	state = OutputImage[ImageIndex] & bitmask;
	
	if (state)
	{
		UartSndString((uint8_t*)"H");
	}
	else
	{
		UartSndString((uint8_t*)"L");
	}
	UartSndString(byteToHexString(outputnumber));
	UartSndString((uint8_t*)"\r\n");
	
}

//returns 0 if the string is not strictly HEX (0-9,a-f,A-f)
uint8_t Parse_isHex(uint8_t* data,uint8_t datalen)
{
	uint8_t i,c;
	
	for (i=0;i<datalen;i++)
	{
		c=data[i];
		if (c >= '0' && c <= '9') continue;
		if (c >= 'a' && c <= 'f') continue;
		if (c >= 'A' && c <= 'F') continue;
		return 0;
	}
	return 1;
}

//returns 0 if the string is not strictly DEC (0-9)
uint8_t Parse_isDec(uint8_t* data,uint8_t datalen)
{
	uint8_t i,c;
	
	for (i=0;i<datalen;i++)
	{
		c=data[i];
		if (c >= '0' && c <= '9') continue;
		return 0;
	}
	return 1;
}

uint8_t Parse_2digitHexToByte(uint8_t* data)
{
	//range already checked '0'-'9', 'a'-'f', 'A'-'F'
	uint8_t retval=0;
	
	retval = Parse_1digitHexToByte(data);
	data+=1;	//advance pointer
	retval<<=4;
	retval |= Parse_1digitHexToByte(data);
	
	return retval;
	
}

uint8_t Parse_1digitHexToByte(uint8_t* data)
{
	if ((*data<='9') && (*data>='0')) return (*data- '0');
	if ((*data<='f') && (*data>='a')) return (*data- 'a' +10);
	if ((*data<='F') && (*data>='A')) return (*data- 'A' +10);
	return 0;
}



