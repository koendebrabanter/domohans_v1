/*
 * sequencer.c
 *
 * Created: 18/11/2018 10:46:45
 *  Author: koen
 */ 
#include <avr/io.h>
#include "AppGlobals.h"
#include "sequencer.h"
#include "app.h"
#include "dataflash.h"

/*
The goal of sequencer is to be sure the correct page from dataflash is loaded and that the data is read and processed (outputs written)
*/

void seq_OnEvent(uint8_t input, uint8_t event)
{
	uint16_t PageNumber;
	uint8_t * pData;
	uint8_t counter;		//counts how much actiona are already performed in this sequence
	uint8_t output;
	uint8_t action;	
	
	//info on dataflash /page organisation is in https://bitbucket.org/koendebrabanter/domohansapp/wiki/DataflashOrganisation
	
	//bail out if event or input is invalid
	if (event >= SEQ_EVENT_INVALID) return;			//valid 0, 1 or 2
	if (input>= INPUTCOUNT) return;					//valid 0-107
	
	//calculate the pagenumber we need to handle this event
	PageNumber = ((input * 3) + event);				//valid 0-323
	
	//Load page from dataflash, unless already loaded.
	if (PageNumber != DF_getActivePage())
	{
		//load the page from flash
		DF_ReadPage(PageNumber);
	}
	
	//get a pointer to the data (it will be always the same pointer, but a nice programmer asks for it in the correct module)
	pData = DF_getBufferPointer();
	
	
	//go trough data, until end, terminator or invalid data.
	counter = 0;	//reset counter before we start the sequence
	while (1)
	{
		action =pData[0];
		output = pData[1];
		
		//bail out if invalid action or terminator is reached.
		if (action == ActionType_NoAction) return;
		if (action == ActionType_Invalid) return;
		
		//we have a valid action: process it
		switch (action)
		{
			case ActionType_Clear:
				ClearOutput(output);
				break;
			case ActionType_Set:
				SetOutput(output);
				break;
			case ActionType_Toggle:
				ToggleOutput(output);
				break;
			case ActionType_Pulse:
				PulseOutput(output);
				break;
			//no support yet for set all and clear all
		}
		
		
		
		//bail out if end of list reached
		counter++;
		if (counter>OUTPUTCOUNT) return;	
		pData +=2; //point to the next element in the actionlist
	}
	
	
}

