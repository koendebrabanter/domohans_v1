/*
 * uart.h
 *
 * Created: 9/08/2018 13:42:28
 *  Author: koen
 */ 


#ifndef UART_H_
#define UART_H_


#define BUFFERSIZE 64

#define ENABLE_COM_SERIAL_INT  		UCSR1B |=   (1<<RXCIE1)|(1<<TXCIE1)			// Enable TX & RX serial interrupt
#define DISABLE_COM_SERIAL_INT		UCSR1B &= ~((1<<RXCIE1)|(1<<TXCIE1))		// Disable TX & RX serial interrupt
#define ENABLE_COM_TX_SERIAL_INT  	UCSR1B |=   (1<<TXCIE1)						// Enable TX serial interrupt
#define DISABLE_COM_TX_SERIAL_INT	UCSR1B &= ~(1<<TXCIE1)						// Disable TX serial interrupt
#define FLUSH_COM_RX				UCSR1B &= ~(1<<RXEN1);	\
									UCSR1B |=  (1<<RXEN1);						// Flush RX by disabling and re-anbling the receiver






void UART_SerialInit(void);
void	StartUartTx(void);
uint8_t ComGetByte(void);
uint8_t ComByteAvailable(void);
void UartSndByte(uint8_t bChar);
void UartSndString(uint8_t * bChar);
void UartSendStatus(void);





#endif /* UART_H_ */