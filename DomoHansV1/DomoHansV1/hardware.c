/*
 * hardware.c
 *
 * Created: 9/08/2018 11:21:33
 *  Author: koen
 */


#include "hardware.h"
#include <avr/interrupt.h>
#include "AppGlobals.h"

volatile uint8_t result;


void Hw_PortsInit(void)
{
	// Ports initialisation
	// The port usage has to be defined in the header file
	DDRA  = DDRA_SETTING;											// Set PortA direction register
	PORTA = PORTA_SETTING;											// Set PortA output register
	
	DDRB  = DDRB_SETTING;											// Set PortB direction register
	PORTB = PORTB_SETTING;											// Set PortB output register
	
	DDRC  = DDRC_SETTING;											// Set PortC direction register
	PORTC = PORTC_SETTING;											// Set PortC output register
	
	DDRD  = DDRD_SETTING;											// Set PortD direction register
	PORTD = PORTD_SETTING;											// Set PortD output register

}



void Hw_TimerInit(void)
{
	//we use 8-bit timer 2 for internal timing
	TCCR2A = 0x00; //no output compare outputs required
	TCCR2B = 0x04; //div64
	TIMSK2 |= BIT0; //interrupt on overflow enable
}




void Hw_DelayMs(uint16_t delay)
{
	uint16_t time = TckVal;
	uint8_t temp;
	while ((TckVal-time)<delay)
	{
		for (temp=0;temp<200;temp++) ASM_NOP;
	}
}

