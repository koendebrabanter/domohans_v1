/*
 * dataflash.c
 *
 * Created: 7/10/2018 20:53:43
 *  Author: koen
 */ 
#include <avr/io.h>
#include "hardware.h"
#include "AppGlobals.h"
#include "dataflash.h"
#include <string.h>

#define DF_PACKETSIZE 146					//maximum usable length of a packet (including space for terminator) (2*72)+2

uint8_t DataFlashPacket[DF_PACKETSIZE];
uint16_t ActivePage=0xffff;					//pagenumber of the page stored in DataFlashPacket
uint8_t PagePointer;						//index to the next byte in DataFlashPacket

/*
connection:
 PD0: DF_SI		RXD0
 PD1: DF_SO		TXD0
 PD4: DF_SCK	
 PD5: DF_RESET
 PD6: DF_CS
 */

/*
registers
data register: UDR0
baud rate register: UBRR0 [baud = Fosc / 2(UBBR +1)]
*/

/**********************************************************
* usage structure
* ---------------
*
* Pages contains 512/528 bytes
* each event on each input is assigned 1 page.
* 108 inputs * 3 events (rise, fall, long) = 324 pages needed
*
* page contains sequential list of commands to be performed.
* 2 bytes per command (action id and output number)
* 
* 72 outputs. so a page can only contain up to 144 bytes
* 
***********************************************************/

#define DF_ACTION_NOACTION00	0x00
#define DF_ACTION_SET			0x01
#define DF_ACTION_CLR			0x02
#define DF_ACTION_SET_ALL		0x03
#define DF_ACTION_CLR_ALL		0x04
#define DF_ACTION_INVALID		0x05

#define DF_OPCODE_STATUS		0xD7


#define PAGEBITS 10

//local prototypes
uint8_t DF_ReadWriteByte(uint8_t data);



void DF_init(void)
{
	// Set MSPI mode of operation and SPI data mode 0. MSB first.
	UCSR0C = (1<<UMSEL01)|(1<<UMSEL00)|(0<<UCSZ00)|(0<<UCPOL0);
	
	//release reset and deselect chip
	DF_RESET_SET;
	DF_CS_SET;
	
	//enable receiver and transmitter
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	
	//set baudrate (must be done after TX is enabled)
	UBRR0 =15;

}
//returns a pointer to the packetbuffer
uint8_t* DF_getBufferPointer(void)
{
	return DataFlashPacket;
}

//returns the number of the page that is loaded in the packetbuffer
uint16_t DF_getActivePage(void)
{
	return ActivePage;
}

//returns the status of the dataflash IC (busy if bit 7 is L)
uint8_t DF_getStatus(void)
{
	volatile uint8_t returnval;
	
	DF_CS_SET;
	DF_CS_CLR;		//select device	
	_NOP();
	
	returnval =  DF_ReadWriteByte(DF_OPCODE_STATUS); //dummy read
	returnval =  DF_ReadWriteByte(0x00); //dummy data
	
	//deselect device
	_NOP();
	DF_CS_CLR;
	DF_CS_SET;	
	return returnval;
	
	//bit 7 H: not busy
	//bit 7 L: busy
	
}

//low leve function to read/write 1 byte on the SPI bus
uint8_t DF_ReadWriteByte(uint8_t data)
{	
	uint8_t retval;
	while ( !( UCSR0A & (1<<UDRE0)) );
	/* Put data into buffer, sends the data */
	UDR0 = data;
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) );
	
	retval = UDR0;
	return retval;		
}


void DF_TestRoutine(void)
{
	uint8_t counter = 0;
	while (1)
	{
		if (TckFlag)
		{
			TckFlag=0;
			counter++;
			if (counter==50)
			{
				counter=0;
			
			LED_TGL;
			//every 2 ms		
			DF_getStatus();
			}
		
		}
	}
}

//this function is used to load a page into RAMbuffer 1, in order to do a read modify write
void DF_Page_To_Buffer(uint16_t PageAdr)
{
	//this action will take 400�s
	
	//xxPPPPPP PPPPPPxx
	DF_CS_SET;		//we need the HtoL transition
	DF_CS_CLR;		//select device
	_NOP();
	
	DF_ReadWriteByte(FlashToBuf1Transfer);							//transfer to buffer 1 op-code
	DF_ReadWriteByte((unsigned char)(PageAdr >> (16 - PAGEBITS)));	//upper part of page address
	DF_ReadWriteByte((unsigned char)(PageAdr << (PAGEBITS - 8)));	//lower part of page address
	DF_ReadWriteByte(0x00);										//don't cares
	
	
	DF_CS_CLR;												//initiate the transfer
	DF_CS_SET;
	
	while(!(DF_getStatus() & 0x80));							//monitor the status register, wait until busy-flag is high
}

//this function is used to store the data from RAMbuffer 1 into flash, at a specific pagenumber, with erase.
void DF_Buffer_To_FlashPage(uint16_t PageNumber)
{
	//up to 40 ms
	//Buf1ToFlashWE
	//xxpppppp ppppppxx xxxxxxxx
	DF_CS_SET;		//we need the HtoL transition
	DF_CS_CLR;		//select device
	_NOP();
	
	DF_ReadWriteByte(Buf1ToFlashWE);							//transfer to buffer 1 op-code
	DF_ReadWriteByte((unsigned char)(PageNumber >> (6)));	//upper part of page address
	DF_ReadWriteByte((unsigned char)(PageNumber << (2)));	//lower part of page address
	DF_ReadWriteByte(0x00);		
	
	_NOP();
	DF_CS_CLR;												//end writing the transfer
	DF_CS_SET;
	
	while(!(DF_getStatus() & 0x80));							//monitor the status register, wait until busy-flag is high
									
}

//this function is used to write data to rambuffer1
void DF_Data_To_Buffer(uint8_t * data, uint8_t datalen)
{
	uint8_t i;
	//requires 3 bytes
	//xxxxxxxxxxxxxxbbbbbbbbbb
	//bbb... =10 buffer address bits (

	DF_CS_SET;		//we need the HtoL transition
	DF_CS_CLR;		//select device
	_NOP();
	
	DF_ReadWriteByte(Buf1Write);								//write to buffer 1 op-code
	DF_ReadWriteByte(0x00);										//we always start to write in the beginning of the page
	DF_ReadWriteByte(0x00);	
	DF_ReadWriteByte(0x00);	
	
	for (i=0;i<datalen;i++)
	{
		DF_ReadWriteByte(*data);
		data++;
	}
	
	
	DF_CS_CLR;												//end writing the transfer
	DF_CS_SET;
	
	while(!(DF_getStatus() & 0x80));							//monitor the status register, wait until busy-flag is high
}

//transfer data from buffer1 to our own packetbuffer
void DF_ReadBuffer(void)
{
	uint8_t i;
	DF_CS_SET;		//we need the HtoL transition
	DF_CS_CLR;		//select device
	_NOP();
	
	DF_ReadWriteByte(Buffer1Read);							//read from buffer 1
	DF_ReadWriteByte(0x00);	//upper part of page address
	DF_ReadWriteByte(0x00);	//lower part of page address
	DF_ReadWriteByte(0x00);
	
	for( i=0; i<DF_PACKETSIZE; i++)
	{
		DataFlashPacket[i] = DF_ReadWriteByte(0x00);			//read 144 times 1 byte while writing a dummy		
	}	
	DF_CS_CLR;
	DF_CS_SET;
}

//do a chip erase
void DF_DeleteAll(void)
{
	//do a chip erase
/*	C7H, 94H, 80H and 9AH*/
	DF_CS_SET;		//we need the HtoL transition
	DF_CS_CLR;		//select device
	_NOP();
	
	DF_ReadWriteByte(0xC7);							
	DF_ReadWriteByte(0x94);	
	DF_ReadWriteByte(0x80);	
	DF_ReadWriteByte(0x9A);
	
	DF_CS_CLR;
	DF_CS_SET;
	
	//while(!(DF_getStatus() & 0x80));
}
/////////////////////////////////////////////////////////////////////////////////
// loading of a page during normal operation
////////////////////////////////////////////////////////////////////////////////
//Reads a page from flash, bypassing the RAM buffer
void DF_ReadPage(uint16_t pagenumber)
{	
	uint8_t i;
	
	//store data in DataFlashPacket and store the pagenumber in ActivePage
	DF_CS_SET;		//we need the HtoL transition
	DF_CS_CLR;		//select device
	_NOP();
	
	
	
	DF_ReadWriteByte(PageRead); //d2
	//24 bit page and byte address sequence
	//byte appress is always 0
	//pppppppp ppppbbbb bbbbbbxx 
	//xxpppppp ppppppbb bbbbbbbb
	DF_ReadWriteByte((unsigned char)(pagenumber >> (6)));	//upper part of page address
	DF_ReadWriteByte((unsigned char)(pagenumber << (2)));	//lower part of page address
	DF_ReadWriteByte(0x00);
	
	// 4 don't care bytes
	DF_ReadWriteByte(0x00);
	DF_ReadWriteByte(0x00);
	DF_ReadWriteByte(0x00);
	DF_ReadWriteByte(0x00);
	
	for( i=0; i<DF_PACKETSIZE; i++)
	{
		DataFlashPacket[i] = DF_ReadWriteByte(0x00);			//read 144 times 1 byte while writing a dummy
	}
	
	ActivePage = pagenumber;
	
	DF_CS_CLR;
	DF_CS_SET;
}

/////////////////////////////////////////////////////////////////////////////////
// storing of a page during configuring
////////////////////////////////////////////////////////////////////////////////


//prepares the packetbuffer to receive new data from the host PC; All data in buffer is lost.
void DF_OpenEmptyBuffer(uint16_t pagenumber)
{
	ActivePage = pagenumber;
	PagePointer=0;	//point to first element of empty page
	//clear page
	memset(DataFlashPacket,0,DF_PACKETSIZE); 
}

//writes 1 action in the already loaded packetbuffer
void DF_AddAction(uint8_t output, uint8_t action)
{
	//write the new action at pointer position an d increase pointer
	//an action contains 2 bytes: action id and output number
	
	DataFlashPacket[PagePointer]= action;
	DataFlashPacket[PagePointer+1]= output;
	//point to next location
	PagePointer+=2;
}

//saves the packetbuffer to flash
void DF_SavePage(void)
{
	//add a dummy action with action id 0 as a terminator
	DataFlashPacket[PagePointer]= ActionType_NoAction;
	DataFlashPacket[PagePointer+1]= 0;
	//point to next location
	PagePointer+=2;
	
	DF_Data_To_Buffer(DataFlashPacket, PagePointer);
	
	//Todo: write to the correct page in flash: pass number as parameter
	DF_Buffer_To_FlashPage(ActivePage);
}